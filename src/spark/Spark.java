package spark;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
public class Spark {
    public static String nombreArchivo = "/Polish.txt";
    public static String lexema="";
    public static int edo = 0, col;
    public static int valorMT;
    public static int token;
    public static int linea = 1,num;
    public static String Val="";
    public static String Tipo="";
    public static nodo pi;
    public static nodo pa;
    public static nodo pf;
    public static boolean band=false;
     public static char[] LeerTexto(String archivo) throws FileNotFoundException, IOException {
        
        String digitos = "";
        FileReader fl = new FileReader(archivo);
        BufferedReader bf = new BufferedReader(fl);
        int ascii, i=0;
        while((ascii = bf.read())!= -1) {
            digitos = digitos + (char)ascii;
            i++;
        }
        digitos = digitos + (char)-1;
        bf.close();
        char[] caracter = digitos.toCharArray();
        return caracter;
    }
     
    public static void crear_nodo()
    {
        pa = new nodo(valorMT, linea, lexema,Tipo,Val);
        if(pi == null)
        {
          pi = pa;
          pf = pi;
        }
        else
        {
            pf.apuntador = pa;
            pf = pa;
        }
    }

            public static void Imprimir(nodo q){
                
                if(q!=null){
                    num++;
                    System.out.println("El lexema es: "+q.lexema+" El token es: "+q.valorMT+" La linea es: "+q.linea+"El tipo es: "+q.tipo+"El valor es: "+q.valor);
                    System.out.println("---------------------------");
                    q=q.apuntador;
                    Imprimir(q);
                }
            }
            public static int PalabraReservada(String pal,int tok){
            tok =0;
            switch(pal){
                case "end":
                    tok=200;
                    pf.tipo ="Palabra reservada";
                    break;
                case "print":
                    tok=201;
                    pf.tipo ="Palabra reservada";
                    break;
                case "while":
                    pf.tipo ="Palabra reservada";
                tok=202;
                break;
                case "do":
                    tok=203;
                    pf.tipo ="Palabra reservada";
                    break;
                case "for":
                    tok =204;
                    pf.tipo ="Palabra reservada";
                    break;
                case "loop":
                    tok =205;
                    pf.tipo ="Palabra reservada";
                    break;
                case "forever":
                    tok =206;
                    pf.tipo ="Palabra reservada";
                    break;
                case "repeat":
                    tok=207;
                    pf.tipo ="Palabra reservada";
                    break;
                case "exit":
                    tok=208;
                    pf.tipo ="Palabra reservada";
                    break;
                case "if":
                    tok =209;
                    pf.tipo ="Palabra reservada";
                    break;
                case "else":
                    tok =210;
                    pf.tipo ="Palabra reservada";
                    break;
                case "then":
                    tok =211;
                    pf.tipo ="Palabra reservada";
                    break;
                case "by":
                    tok=212;
                    pf.tipo ="Palabra reservada";
                    break;
                case"true":
                    tok=213;
                    pf.tipo ="Palabra reservada";
                    break;
                case"false":
                    tok=214;
                    pf.tipo ="Palabra reservada";
                    break;
                case "and":
                    tok=215;
                    pf.tipo ="Palabra reservada";
                    break;
                case "or":
                    tok=216;
                    pf.tipo ="Palabra reservada";
                    break;
                case "not":
                    tok=217;
                    pf.tipo ="Palabra reservada";
                    break;
                case "procedure":
                    tok=218;
                    pf.tipo ="Palabra reservada";
                    break;
                case "until":
                    tok=219;
                    pf.tipo ="Palabra reservada";
                    break;
                case "read":
                    tok=220;
                    pf.tipo ="Palabra reservada";
                    break;
                case "to":
                    tok=221;
                    pf.tipo ="Palabra reservada";
                    break;
                case "begin":
                    tok=222;
                    pf.tipo ="Palabra reservada";
                    break;
                case "go":
                    tok=223;
                    pf.tipo ="Palabra reservada";
                    break;
                case "int":
                    tok=250;
                    pf.tipo ="entero";
                    break;
                case "string":
                    tok=251;
                    pf.tipo="cadena";
                    break;
                case "doble":
                    tok=252;
                    pf.tipo="decimal";
                    break;
                case "Leer":
                    pf.tipo="Lectura";
                    tok=300;
                    break;
                default:
               tok=100;
            }
            return tok;
        }
     public static String Caracter(int ValorMT,String lexema){   
            switch(ValorMT){
                case 103:
                    lexema="+";
                    
                    break;
                case 104:
                    lexema="-";
                 
                    break;
                case 105:
                    lexema="*";
                    
                    break;
                case 106:
                    lexema="/";
                    
                    break;
                case 110:
                    lexema=">=";
                    
                    break;
                case 111:
                    lexema="<=";
                    
                    break;
                case 112:
                    lexema="!=";
                    
                    break;
                case 120:
                    lexema=":=";
                    
                    break;
                case 114:
                    lexema=";";
                    
                    break;
                case 117:
                    lexema="(";
                    
                    break;
                case 118:
                    lexema=")";
                    
                    break;
                      
            }
            return lexema;
        }
     public static String Errores(int token,String error){
         switch (token){
             case 500:
                 error = "Se esperaba un numero";
                 break;
             case 501:
                 error = "EOF inesperado";
                 break;
             case 502:
                 error ="se esperaba un =";
                 break;
             case 503:
                 error ="Simbolo invalido";
                 break;  
         }
         return error;
     }
    public static void main(String[] args) throws IOException {
        String directorio = System.getProperty("user.dir");
         char[] caracter = LeerTexto(directorio+nombreArchivo);
            for (int i = 0; i < caracter.length; i++) {
            switch (caracter[i]){
               case'a' : case'b': case'c':
               case'd' : case'e': case'f':
               case'g' : case'h': case'i':
               case'j' : case'k': case'l':
               case'm' : case'n': case'ñ':
               case'o' : case'p': case'q':
               case'r' : case's': case't':
               case'u' : case'w': case'x':    
               case'y' : case'z': case'v':   
               case'A' : case'B': case'C':
               case'D' : case'E': case'F':
               case'G' : case'H': case'I':
               case'J' : case'K': case'L':
               case'M' : case'N': case'Ñ':
               case'O' : case'P': case'Q':
               case'R' : case'S': case'T':
               case'U' : case'W': case'X':    
               case'Y' : case'Z': case'V':
                   col = 0;
                   break;
               case'1': case'2':
               case'3': case'4': 
               case'5': case'6': 
               case'7': case'8': 
               case'9': case'0':
                   col = 1;
                   break;
               case'+':
                   col = 2;
                   band =true;
                   pf.tipo ="Operador aritmetico";
                    break;
               case'-':
                   col = 3;
                   pf.tipo ="Operador aritmetico";
                    break;      
               case'*':
                   col = 4;
                   pf.tipo ="Operador aritmetico";
                    break;   
               case'/':
                   col = 5;
                   pf.tipo ="Operador aritmetico";
                    break;
               case'>':
                   col = 6;
                   pf.tipo ="Operador Relacional";
                    break;        
               case'<':
                   pf.tipo ="Operador Relacional";
                   col = 7;
                    break;
               case'=':
                   pf.tipo ="Operador Relacional";
                   col = 8;
                    break;
               case'!':
                   pf.tipo ="Operador Relacional";
                   col = 9;
                    break;
               case'(':
                   pf.tipo ="Simbolo de agrupacion";
                   col = 10;
                    break;
               case')':
                   pf.tipo ="Simbolo de agrupacion";
                   col = 11;
                    break;
               case'.':
                   col = 12;
                    break;
               case';':
                   col = 13;
                    break;
               case ((char) 39):
                   col = 14;
                    break;
               case'"':
                   col = 15;
                    break;
               case':':
                   col = 16;
                    break;
               case ((char)32):
                   col = 17;
                   break;
               case ((char)13):
                   col = 18;
                   break;
               case ((char)10):
                   linea++;
                   col = 19;
                   break;
               case ((char)9):
                   col = 20;
                   break;
               case ((char)-1):
                   col = 21;
                   break;
               default:
                   col = 22;
                   break;
           }
            MatrizT mt = new MatrizT();
            valorMT = mt.matriz[edo][col];
            if (valorMT < 100){
                    edo = valorMT;
                    if(valorMT!=0){
                    lexema+=caracter[i];
                    }
            }if(valorMT==7){
            lexema="";
            }
            else if (valorMT >= 100 && valorMT < 200){
                   crear_nodo();
                   if(valorMT==100){
                   pa = pa.apuntador;
                   int nv=PalabraReservada(lexema,valorMT);
                   valorMT=nv;
                   token=valorMT;
                   pf.valorMT=token;
                   pf.valor =lexema;
                   i--;
                   }
                   if(valorMT==107 || valorMT==108||valorMT==101 || valorMT==102 || valorMT==106){
                   i--;
                   }
                   String nl=Caracter(valorMT,lexema);
                   lexema=nl;
                   lexema+=caracter[i];
                   pf.lexema=nl;
                   lexema="";
                   edo=0;
            }else if(valorMT>=500){   
            String error ="";   
            String mensaje =Errores(valorMT,error);
                System.out.println(mensaje+" En la linea "+linea);
                lexema="";
                band=true;
                break;
            }else{
                    band=false;
            }
      }
            if(band==false){
                valorMT=500;
                lexema="final";
                linea =500;
            Sintaxis sx = new Sintaxis(pi);
            sx.ImprimeEnsa();
           
            }
    }
}
