
package spark;
public class MatrizT {
      public int [][] matriz = {
        /*0     1       2       3       4       5       6       7       8       9       10      11      12      13      14      15      16      17      18      19      20      21      22*/           
/*Q/S	Letra	Digito	+	-	*	/	>	<	=	!	(	)	.	;	'	''	:	eb	eol	nl	tab	eof	oc*/
/*0 */  {1,      2,     103,    104,    105,    5,      8,       9,    503,     10,      117,   118,    503,    114,    115,     11,     12,     0,      0,      0,      0,      0,     503},    
/*1*/   {1,	1,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	100,	503},
/*2*/   {101,	2,	101,	101,	101,	101,	101,	101,	101,	101,	101,	101,	3,	101,	101,	101,	101,	101,	101,	101,	101,	101,	503},
/*3*/   {500,	4,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	500,	503},
/*4*/   {102,	4,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	102,	503},
/*5*/   {106,	106,	106,	106,	106,	6,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	106,	503},
/*6*/   {6,	6,	6,	6,	6,	7,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	501,	6},
/*7*/   {6,	6,	6,	6,	6,	0,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	6,	501,	6},
/*8*/   {107,	107,	107,	107,	107,	107,	107,	107,	110,	107,	107,	107,	107,	107,	107,	107,	107,	107,	107,	107,	107,	107,	503},
/*9*/   {108,	108,	108,	108,	108,	108,	108,	108,	111,	108,	108,	108,	108,	108,	108,	108,	108,	108,	108,	108,	108,	108,	503},
/*10*/  {502,	502,	502,	502,	502,	502,	502,	502,	112,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	503},
/*11*/  {11,	11,	11,	11,	11,	11,	11,	11,	11,	11,	11,	11,	11,	11,	11,	119,	11,	11,	501,	11,	11,	501,	503},
/*12*/  {502,	502,	502,	502,	502,	502,	502,	502,	120,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	502,	503}
    };
}

