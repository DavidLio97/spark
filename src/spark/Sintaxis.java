package spark;

import java.io.File;
import java.io.FileWriter;
import java.util.Vector;

public class Sintaxis {

    public static int contador = 1;
    public static nodo pi;
    public static nodo pa;
    public static nodo pf;
    public static MatrizT mt;
    public static Temporales P1;
    public static Temporales Q1;
    public static Temporales F1;
    public static Vector<String> Variables = new Vector<String>();
    public static Vector<String> VariablesTipo = new Vector<String>();
    public static Vector<String> VariablesCadena = new Vector<String>();
    public static Vector<String> VariablesValor = new Vector<String>();
    public static Vector<String> VT = new Vector<String>();
    public static Vector<String> VA = new Vector<String>();
    public static Vector<String> VD = new Vector<String>();
    public static Vector<String> Vinf = new Vector<String>();
    public static Vector<String> Vint = new Vector<String>();
    public static Vector<String> VI = new Vector<String>();
    public static Vector<String> VTI = new Vector<String>();
    public static Vector<String> Polish = new Vector<String>();
    public static boolean bandera;
    public static int cont = 0;
    public static int temporal = 0;
    public static int NumeroTemporal = 0;

    public static Polaca X = null;
    public static Polaca Y = null;
    public static Polaca Z = null;
    public static auxiliar P = null;
    public static auxiliar Q = null;
    public static auxiliar R = null;
    public static int prioridad;

    public Sintaxis(nodo p) {
        pi = p;
        pa = pi;
        pf = pa;
        spark();
    }

    public static void spark() {
        if (pa.valorMT == 218) {
            pa = pa.apuntador;
            if (pa.valorMT == 100) {
                pa = pa.apuntador;
                if (pa.valorMT == 114) {
                    pa = pa.apuntador;
                    bloque();
                } else {
                    System.out.println("falta ;" + "en la linea" + pa.linea);
                }
            } else {
                System.out.println("falta id" + "en la linea" + pa.linea);
            }
        } else {
            System.out.println("falta procedure" + "en la linea" + pa.linea);
        }
        Imp_polaca(X);
    }

    public static void bloque() {
        if (pa.valorMT == 222) {
            pa = pa.apuntador;
            sentencia();
            if (pa.valorMT == 200) {
                pa = pa.apuntador;
                if (pa.valorMT == 114) {
                    pa = pa.apuntador;
                } else {
                    System.out.println("falta ;" + "en la linea" + pa.linea);
                }
            } else {
                System.out.println("falta end" + "en la linea" + pa.linea);
            }
        } else {
            System.out.println("falta begin" + "en la linea" + pa.linea);
        }

    }

    public static void sentencia() {
        switch (pa.valorMT) {
            case 209:
                pa = pa.apuntador;
                if (pa.valorMT == 100 || pa.valorMT == 101) {
                    if (Variables.contains(pa.lexema) == true || pa.valorMT == 101) {
                        expresion_logica();
                        //Brinco si es falso a p
                        CrearPolaca("BFP", 810);
                    } else if (Variables.contains(pa.lexema) == false) {
                        System.out.println("Variable no declarada: " + pa.lexema + " en la linea: " + pa.linea);
                        System.exit(0);

                    }
                } else {
                    System.out.println("Se esperaba una variable en la linea  " + pa.linea);
                    System.exit(0);
                }
                if (pa.valorMT == 211) {
                    pa = pa.apuntador;
                    sentencia();
                    //Brinco incondicional a Q
                    CrearPolaca("BIQ", 820);
                    if (pa.valorMT == 210) {
                        pa = pa.apuntador;
                        CrearPolaca("P", 811);
                        sentencia();
                        if (pa.valorMT == 200) {
                            CrearPolaca("Q", 821);
                            pa = pa.apuntador;
                        } else {
                            System.out.println("falta end" + "en la linea" + pa.linea);
                        }
                    } else {
                        if (pa.valorMT == 200) {
                            CrearPolaca("Q", 821);
                            CrearPolaca("P", 811);
                            pa = pa.apuntador;
                        } else {
                            System.out.println("falta end" + "en la linea" + pa.linea);
                        }
                    }
                } else {
                    System.out.println("falta then" + "en la linea" + pa.linea);
                }//se esperaba un then
                break;
            case 202:
                CrearPolaca("S", 841);
                pa = pa.apuntador;
                if (pa.valorMT == 100 || pa.valorMT == 101) {
                    if (Variables.contains(pa.lexema) == true || pa.valorMT == 101) {
                        expresion_logica();
                        //Brinco si es falso a R
                        CrearPolaca("BFR", 830);
                    } else if (Variables.contains(pa.lexema) == false) {
                        System.out.println("Variable no declarada: " + pa.lexema + " en la linea: " + pa.linea);
                        System.exit(0);

                    }
                } else {
                    System.out.println("Se esperaba una variable en la linea  " + pa.linea);
                    System.exit(0);
                }
                if (pa.valorMT == 203) {
                    pa = pa.apuntador;
                    sentencia();
                    //Brinco incondicional a S
                    CrearPolaca("BIS", 840);
                    if (pa.valorMT == 200) {
                        CrearPolaca("R", 831);
                        pa = pa.apuntador;
                    } else {
                        System.out.println("falta end" + "en la linea" + pa.linea);
                    }
                } else {
                    System.out.println("falta do" + "en la linea" + pa.linea);
                }// se esperaba do 
                break;
            case 207:
                CrearPolaca("S", 841);
                pa = pa.apuntador;
                if (pa.valorMT == 100 || pa.valorMT == 101) {
                    if (Variables.contains(pa.lexema) == true || pa.valorMT == 101) {
                        expresion_logica();
                        //Brinco si es falso a R
                        CrearPolaca("BFR", 830);
                    } else if (Variables.contains(pa.lexema) == false) {
                        System.out.println("Variable no declarada: " + pa.lexema + " en la linea: " + pa.linea);
                        System.exit(0);

                    }
                } else {
                    System.out.println("Se esperaba una variable en la linea  " + pa.linea);
                    System.exit(0);
                }
                if (pa.valorMT == 203) {
                    pa = pa.apuntador;
                    sentencia();
                    //Brinco incondicional a S
                    CrearPolaca("BIS", 840);
                    if (pa.valorMT == 200) {
                        CrearPolaca("R", 831);
                        pa = pa.apuntador;
                    } else {
                        System.out.println("falta end" + "en la linea" + pa.linea);
                    }
                } else {
                    System.out.println("falta do" + "en la linea" + pa.linea);
                }// se esperaba do 
                break;
            case 205:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                sentencia();
                if (pa.valorMT == 206) {
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                } else {
                    System.out.println("falta foverer" + "en la linea" + pa.linea);
                }
                break;
            case 208:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                break;
            case 204:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                if (pa.valorMT == 250) {
                    pa = pa.apuntador;
                    if (pa.valorMT == 100) {
                        if (Variables.contains(pa.lexema) == false) {
                            Variables.add(pa.lexema);
                            VariablesTipo.add("int");
                            Vint.add(pa.lexema);
                        } else if (Variables.contains(pa.lexema) == true) {
                            System.out.println("Variable duplicada: " + pa.lexema + " en la linea: " + pa.linea);
                            System.exit(0);

                        }
                    } else {
                        System.out.println("Se esperaba una variable en la linea  " + pa.linea);
                        System.exit(0);
                    }
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 120) {
                        Ingresar_Polish(pa);
                        pa = pa.apuntador;
                        if (pa.valorMT == 101) {
                            Ingresar_Polish(pa);
                            pa = pa.apuntador;
                            if (pa.valorMT == 221) {
                                Ingresar_Polish(pa);
                                pa = pa.apuntador;
                                if (pa.valorMT == 101) {
                                    Ingresar_Polish(pa);
                                    pa = pa.apuntador;
                                    if (pa.valorMT == 212) {
                                        Ingresar_Polish(pa);
                                        pa = pa.apuntador;
                                        if (pa.valorMT == 101) {
                                            Ingresar_Polish(pa);
                                            pa = pa.apuntador;
                                            if (pa.valorMT == 203) {
                                                Ingresar_Polish(pa);
                                                pa = pa.apuntador;
                                                sentencia();
                                                if (pa.valorMT == 200) {
                                                    Ingresar_Polish(pa);
                                                    pa = pa.apuntador;
                                                } else {
                                                    System.out.println("falta end" + "en la linea" + pa.linea);
                                                }//se esperaba end
//                                                
                                            } else {
                                                System.out.println("falta do" + "en la linea" + pa.linea);
                                            }//se esperaba do
                                        } else {
                                            System.out.println("falta num entero" + "en la linea" + pa.linea);
                                        }//se esperaba Num Enteor
                                    } else {
                                        System.out.println("falta by" + "en la linea" + pa.linea);
                                    }//se esperaba by
                                } else {
                                    System.out.println("falta num entero" + "en la linea" + pa.linea);
                                }//se esperaba Num Entero
                            } else {
                                System.out.println("falta do" + "en la linea" + pa.linea);
                            }//se esperaba to
                        } else {
                            System.out.println("falta num entero" + "en la linea" + pa.linea);
                        }//se esperaba Num Entero
                    } else {
                        System.out.println("falta :=" + "en la linea" + pa.linea);
                    }//se esperaba :=
                } else {
                    System.out.println("falta id" + "en la linea" + pa.linea);
                }//se esperaba ID
                break;
            case 201:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                if (pa.valorMT == 100 || pa.valorMT == 101) {
                    if (pa.valorMT == 100) {
                        if (Variables.contains(pa.lexema) == false) {
                            System.out.println("Variable no declarada: " + pa.lexema + " en la linea: " + pa.linea);
                            System.exit(0);
                        }
                    } else {
                        System.out.println("Se esperaba una variable en la linea  " + pa.linea);
                        System.exit(0);
                    }
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 103) {
                        concatenar();
                    }
                } else if (pa.valorMT == 119) {
                    cont++;
                    VariablesCadena.add(pa.lexema);

                    VT.add("cadena" + cont);
                    valor_cadena();
                } else {
                    System.out.println("Se esperaba cadena");
                    System.exit(0);
                }
                Vaciar_Pila();
                break;
            case 220:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                if (pa.valorMT == 100) {
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                } else {
                    System.out.println("faltaba id" + "en la linea" + pa.linea);
                }//se esperaba ID
                break;
            case 223:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                if (pa.valorMT == 221) {
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 101) {
                        Ingresar_Polish(pa);
                        pa = pa.apuntador;
                    } else {
                        System.out.println("falta num entero" + "en la linea" + pa.linea);
                    }//se esperaba Num Entero
                } else {
                    System.out.println("falta to" + "en la linea" + pa.linea);
                }//se esperaba un to
                break;
            case 250:
                pa = pa.apuntador;
                if (pa.valorMT == 100) {
                    if (Variables.contains(pa.lexema) == true) {
                        System.out.println("Variable duplicada" + pa.lexema + " en la linea: " + pa.linea);
                    } else if (Variables.contains(pa.lexema) == false) {
                        Variables.add(pa.lexema);
                        VariablesTipo.add("int");
                        Vint.add(pa.lexema);
                    }
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 120) {
                        Ingresar_Polish(pa);
                        pa = pa.apuntador;
                        if (pa.valorMT == 101 || pa.valorMT == 102 || Vint.contains(pa.lexema) == true) {
                            VariablesValor.add(pa.lexema);
                            Vinf.add(pa.lexema);
                            VI.add(pa.lexema);
                            temporal++;
                            VTI.add("NumTemp" + temporal);
                            expresion_numerica();
                        } else {
                            System.out.println("Se esperaba un numero" + " linea " + pa.linea);
                        }
                    } else {
                        System.out.println("falta :=" + "en la linea" + pa.linea);
                    }//se esperaba :=
                } else {
                    System.out.println("Falta un identificador en la linea" + pa.linea);
                }
                Vaciar_Pila();
                break;
            case 251:
                pa = pa.apuntador;
                if (pa.valorMT == 100) {
                    if (Variables.contains(pa.lexema) == true) {
                        System.out.println("Variable duplicada" + " en la linea: " + pa.linea);
                    } else if (Variables.contains(pa.lexema) == false) {
                        Variables.add(pa.lexema);
                        VariablesTipo.add("string");
                        VariablesCadena.add(pa.lexema);
                    }
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 120) {
                        Ingresar_Polish(pa);
                        pa = pa.apuntador;
                        if (pa.valorMT == 119) {
                            cont++;
                            VariablesCadena.add(pa.lexema);
                            VariablesValor.add(pa.lexema);
                            Vinf.add(pa.lexema);
                        }
                        valor_cadena();
                    } else {
                        System.out.println("Se esperaba un := en la linea " + pa.linea);
                    }
                } else {
                    System.out.println("Falta un identificador en la linea: " + pa.linea);
                }
                Vaciar_Pila();
                break;
            case 100:
                if (pa.valorMT == 100) {
                    if (Variables.contains(pa.lexema) == true) {
                        String temporal = pa.lexema;
                        Ingresar_Polish(pa);
                        pa = pa.apuntador;
                        if (pa.valorMT == 103) {
                            Ingresar_Polish(pa);
                            pa = pa.apuntador;
                            if (pa.valorMT == 103) {
                                int numero1 = Variables.indexOf(temporal, NumeroTemporal);
                                String n = VariablesValor.get(numero1);
                                int num = Integer.parseInt(n);

                                pa = pa.apuntador;
                                break;
                            } else {
                                System.out.println("Se esperaba un incremento");
                                System.exit(0);
                            }
                        } else if (pa.valorMT == 104) {
                            Ingresar_Polish(pa);
                            pa = pa.apuntador;
                            if (pa.valorMT == 104) {
                                int numero1 = Variables.indexOf(temporal, NumeroTemporal);
                                String n = VariablesValor.get(numero1);
                                int num = Integer.parseInt(n);

                                pa = pa.apuntador;
                                break;
                            } else {
                                System.out.println("Se esperaba un decremento");
                                System.exit(0);
                            }
                        }
                    } else if (Variables.contains(pa.lexema) == false) {
                        System.out.println("Variable no declarada " + pa.lexema + " en la linea " + pa.linea);
                        System.exit(0);
                    }

                    if (pa.valorMT == 120) {
                        Ingresar_Polish(pa);
                        pa = pa.apuntador;
                        expresion_numerica();
                    } else {
                        System.out.println("falta :=" + "en la linea" + pa.linea);
                    }//se esperaba :=
                } else {
                    System.out.println("Falta un identificador en la linea" + pa.linea);
                }
                Vaciar_Pila();
                break;
            case 300:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                if (pa.valorMT == 250) {
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 100) {
                        if (Variables.contains(pa.lexema) == true) {
                            System.out.println("Variable duplicada");
                            System.exit(0);
                        } else {
                            Variables.add(pa.lexema);
                            VariablesValor.add("0");
                            VariablesTipo.add("int");
                            Ingresar_Polish(pa);
                            pa = pa.apuntador;
                        }
                    } else {
                        System.out.println("Se esperaba una variable");
                        System.exit(0);
                    }
                } else {
                    System.out.println("Se esperaba una declaracion");
                    System.exit(0);
                }
                if (pa.valorMT == 251) {
                    Ingresar_Polish(pa);
                    pa = pa.apuntador;
                    if (pa.valorMT == 100) {
                        if (Variables.contains(pa.lexema) == true) {
                            System.out.println("Variable duplicada");
                            System.exit(0);
                        } else {
                            Variables.add(pa.lexema);
                            VariablesValor.add("0");
                            VariablesTipo.add("String");
                            Ingresar_Polish(pa);
                            pa = pa.apuntador;
                        }
                    } else {
                        System.out.println("Se esperaba una variable");
                        System.exit(0);
                    }
                }
                break;
            default:
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                break;
        }
        if (pa.valorMT == 300 || pa.valorMT == 250 || pa.valorMT == 251 || pa.valorMT == 209 || pa.valorMT == 202 || pa.valorMT == 207 || pa.valorMT == 205 || pa.valorMT == 208 || pa.valorMT == 204 || pa.valorMT == 201 || pa.valorMT == 220 || pa.valorMT == 223 || pa.valorMT == 100 || pa.valorMT == 119) {
            sentencia();
        }
    }

    public static void expresion_logica() {
        switch (pa.valorMT) {
            case 213:
            case 214:
                valor_logico();
                expresion_logica1();
                break;
            case 217:
                operador_logico1();
                expresion_logica();
                expresion_logica1();
                break;
            case 215:
            case 216:
                expresion_logica();
                expresion_logica1();
                break;
            default:
                expresion_relacional();
                expresion_logica1();
                break;
        }
        Vaciar_Pila();
    }

    public static void expresion_logica1() {
        if (pa.valorMT == 216 || pa.valorMT == 215) {
            operador_logico2();
            expresion_logica();
            expresion_logica1();
        }
    }

    public static void valor_cadena() {
        if (pa.valorMT == 119) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
            if (pa.valorMT == 103) {
                Ingresar_Polish(pa);
                pa = pa.apuntador;
                if (pa.valorMT == 100 || pa.valorMT == 119) {
                    concatenar();
                } else {
                    System.out.println("Se esperaba variable o cadena en la linea " + pa.linea);
                    System.exit(0);
                }
            }
        } else {
            System.out.println("falta cadena o variable" + " en la linea " + pa.linea);//se esperaba cadena
        }
    }

    public static void expresion_numerica() {
        switch (pa.valorMT) {
            case 100:
                if (pa.valorMT == 100) {
                    if (Variables.contains(pa.lexema) == true) {
                        if (Vint.contains(pa.lexema) == true) {
                            Ingresar_Polish(pa);
                            pa = pa.apuntador;
                            expresion_numerica1();
                        } else {
                            System.out.println("Se esperaba un entero en la linea " + pa.linea);
                            System.exit(0);
                        }
                    } else if (Variables.contains(pa.lexema) == false) {
                        System.out.println("Variable no declarada: " + pa.lexema + " Em la linea: " + pa.linea);
                        System.exit(0);
                    }
                }
                break;
            case 101:
            case 102:
                valor_numerico();
                expresion_numerica1();
                break;
            case 213:
            case 214:
                valor_logico();
                expresion_numerica1();
                break;
            case 119:
                valor_cadena();
                expresion_numerica1();
                break;
            case 103:
            case 104:
            case 105:
            case 106:
                expresion_numerica1();
                break;
            default:
                if (pa.valorMT >= 200) {
                    System.out.println("Falta expresion numerica" + "en la linea" + pa.linea);
                } else {
                    expresion_numerica();
                    expresion_numerica1();
                }
                break;
        }
    }

    public static void expresion_relacional() {
        expresion_numerica();
        operador_relacional();
        expresion_numerica();
    }

    public static void expresion_numerica1() {
        if (pa.valorMT == 103 || pa.valorMT == 104 || pa.valorMT == 105 || pa.valorMT == 106) {
            operador_numerico();
            expresion_numerica();
            expresion_numerica1();
        }
    }

    public static void valor_numerico() {
        if (pa.valorMT == 101 || pa.valorMT == 102) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else {
            System.out.println("falta valor numerico" + "en la linea" + pa.linea);//falto valor numerico
        }
    }

    public static void valor_logico() {
        if (pa.valorMT == 215 || pa.valorMT == 216) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else {
            System.out.println("falta valor logico" + "en la linea" + pa.linea); //falta valor logico
        }
    }

    public static void operador_logico2() {
        if (pa.valorMT == 215 || pa.valorMT == 216) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else {
            System.out.println("falta or o and" + "en la linea" + pa.linea); //se esperaba or o and
        }
    }

    public static void operador_relacional() {
        if (pa.valorMT == 107 || pa.valorMT == 108 || pa.valorMT == 109 || pa.valorMT == 108 || pa.valorMT == 109 || pa.valorMT == 110 || pa.valorMT == 111 || pa.valorMT == 112) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else {
            System.out.println("falta operador relacional" + "en la linea" + pa.linea); //faltaba operador relacional
        }
    }

    public static void operador_numerico() {
        if (pa.valorMT == 103 || pa.valorMT == 104 || pa.valorMT == 105 || pa.valorMT == 106) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else {
            System.out.println("falta operador numerico" + "en la linea" + pa.linea);//faltaba operador numerico
            System.exit(0);
        }
    }

    public static void operador_logico1() {
        if (pa.valorMT == 218) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else {
            System.out.println("falta not" + "en la linea" + pa.linea);  //Se esperaba not
        }
    }
//    public static nodo Ingresar_Polaca(nodo s){
//        
//    }

    public static void CrearPolaca(String lexema, int token) {
        if (X == null) {
            X = new Polaca(lexema, token);
            Y = Z = X;
        } else {
            Y = new Polaca(lexema, token);
            Z.liga = Y;
            Z = Y;
        }
    }

    public static void RecorrerAux() {
        if (Q.liga != R) {
            Q = Q.liga;
            RecorrerAux();
        }
    }

    public static void Vaciar_Pila() {
        if (R != null) {
            if (R.token == 117) {
                Q = P;
                RecorrerAux();
                Q.liga = null;
                R = Q;
            } else {
                if (R != null) {
                    Y = new Polaca(R.lexema, R.token);
                    Z.liga = Y;
                    Z = Y;
                    Q = P;
                    RecorrerAux();
                    Q.liga = null;
                    R = Q;
                    if (R.token != 1000) {
                        Vaciar_Pila();
                    } else {
                        P = Q = R = null;
                    }
                }
            }
        }

    }

    public static int Prioridades(int token) {
        switch (token) {
            case 215:
            case 216:
            case 217:
                prioridad = 1;
                break;
            case 108:
            case 107:
            case 109:
            case 110:
            case 111:
            case 112:
                prioridad = 2;
                break;
            case 120:
                prioridad = 3;
                break;
            case 103:
            case 104:
                prioridad = 4;
                break;
            case 105:
            case 106:
                prioridad = 5;
                break;
            case 117:
                prioridad = 7;
                break;
            case 118:
                prioridad = 8;
                break;
            case 300:
            case 201:
                prioridad = 9;
                break;
            case 800:
                prioridad = 10;
                break;
        }
        return prioridad;
    }

    public static void Imp_Aux(auxiliar Imp) {
        if (Imp != null) {
            System.out.println("Auxiliar: " + Imp.lexema);
            Imp_Aux(Imp.liga);
        }
    }

    public static void Imp_polaca(Polaca Pol) {
        if (Pol != null) {
            System.out.println("Polish: " + Pol.lexema);
            Imp_polaca(Pol.liga);
        }
    }
    public static String vari = "";
    public static String vari1 = "";

    public static nodo Ingresar_Polish(nodo S) {
        if (S.valorMT == 100 || S.valorMT == 101 || S.valorMT == 119) {
            if (X == null) {
                X = new Polaca(S.lexema, S.valorMT);
                Y = Z = X;
            } else {
                Y = new Polaca(S.lexema, S.valorMT);
                Z.liga = Y;
                Z = Y;
            }
        } else {
            prioridad = Prioridades(S.valorMT);
            if (P == null) {
                P = new auxiliar("TOPE", 1000, 0);
                Q = new auxiliar(S.lexema, S.valorMT, prioridad);
                P.liga = Q;
                R = Q;
            } else {
                if (S.valorMT == 117) {
                    Q = new auxiliar(S.lexema, S.valorMT, prioridad);
                    R.liga = Q;
                    R = Q;
                } else {
                    if (S.valorMT == 118) {
                        Vaciar_Pila();
                    } else {
                        if (prioridad > R.prioridad) {
                            Q = new auxiliar(S.lexema, S.valorMT, prioridad);
                            R.liga = Q;
                            R = Q;
                        } else {
                            if (R.token != 117) {
                                Y = new Polaca(R.lexema, R.token);
                                Z.liga = Y;
                                Z = Y;
                                Q = P;
                                RecorrerAux();
                                Q.liga = null;
                                R = Q;
                                S = Ingresar_Polish(S);
                            } else {
                                Q = new auxiliar(S.lexema, S.valorMT, prioridad);
                                R.liga = Q;
                                R = Q;
                            }
                        }
                    }
                }
            }
        }

        return S;
    }

    public void Polish_Ensablador() {
        if (X != null) {
            switch (X.token) {
                case 100:
                    if (X.liga.lexema != null && X.liga.token == 201) {
                        metodos = metodos + "\r\n mov ax,seg " + X.lexema + "\n"
                                + " mov ds,ax\n"
                                + " \n"
                                + " mov ah,09\n"
                                + " lea dx," + X.lexema + "\n"
                                + " int 21h"
                                + "\r\n call nuevaLinea";

                    }
                    if (X.liga.lexema != null && X.liga.token == 119) {
                        variable1 = variable1 + "\r\n" + X.lexema + " db " + X.liga.lexema + "\",\"$\" ";
                        break;
                    }
                    if (X.liga.lexema != null && X.liga.token == 101) {
                        variable1 = variable1 + "\r\n" + X.lexema + " db " + X.liga.lexema + ",\"$\" ";
                        break;
                    }
                    break;
                case 101:
                    variable1 = variable1 + "\r\n" + "T" + contador + " db " + X.lexema + ",\"$\" ";
                    contador++;
                    break;

                case 107:
                    if (X.liga.token == 810) {
                        condicion = "\r\n jnz P";
                    } else if (instruccion.equals("R")) {
                        condicion = "\r\n jc R";
                    }
                    break;
                case 108:
                    if (X.liga.token == 810) {
                        condicion = "\r\n jc P";
                    } else if (instruccion.equals("R")) {
                        condicion = "\r\n jnz R";
                    }
                    break;
                case 810:
                    metodos = metodos
                            + "\r\n mov al, T" + (contador - 2)
                            + "\r\n cmp al, T" + (contador - 1)
                            + condicion;
                    break;

                case 820:
                    metodos = metodos
                            + "\r\n jmp Q";
                    break;
                case 811:
                    metodos = metodos
                            + "\r\n P:";
                    break;
                case 821:
                    metodos = metodos
                            + "\r\n Q:";
                    break;
                case 841:
                    instruccion = "R";
                    metodos = metodos
                            + "\r\n S:";
                    break;
                case 840:
                    metodos = metodos
                            + "\r\n jmp S";
                    break;
                case 830:
                    metodos = metodos
                            + "\r\n mov al, T" + (contador - 2)
                            + "\r\n cmp al, T" + (contador - 1)
                            + condicion;
                    break;
                case 831:
                    metodos = metodos
                            + "\r\n R:";
                    break;
            }
            X = X.liga;
            Polish_Ensablador();
        }
    }

    String Header;
    String Body;
    String Footer;
    public static String instruccion = "";
    public static String PE = "";
    public static String variable1 = "";
    public static String metodos = "";
    public static String condicion = "";
    public static String ciclos = "\r\n nuevaLinea proc\n"
            + "mov dl, 10\n"
            + " mov ah,02h\n"
            + " int 21h\n"
            + " mov dl,13\n"
            + " mov ah, 02h\n"
            + " int 21h \n"
            + " ret\n"
            + "nuevaLinea endp";

    public void ImprimeEnsa() {
        Polish_Ensablador();
        Header = ".MODEL SMALL"
                + " \r\n .stack "
                + "\r\n .data ";
        Body = "\r\n .code ";
        Footer = "main endp"
                + " \r\n end main";
        String CodigoCompleto = Header + "\r\n" + variable1 + "\r\n" + "\r\n" + "\r\n" + Body + ciclos + "\r\n\r\n main proc" + "\r\n " + metodos + PE + "\r\n\r\n" + Footer;
        GuardaEnsamblador(CodigoCompleto);
    }

    public void GuardaEnsamblador(String Cadena) {
        try {
            String directorio = System.getProperty("user.dir");
            String ruta = directorio + "/MACROS/programa.asm";
            File archivo = new File(ruta);
            FileWriter escribir = new FileWriter(archivo, false);
            escribir.write(Cadena);
            escribir.close();
        } catch (Exception e) {
            System.out.println("Error al guardar archivo ASM");
        }
    }

    public static void concatenar() {
        if (Variables.contains(pa.lexema) == true) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
        } else if (Variables.contains(pa.lexema) == false) {
            System.out.println("Variable no declarada: " + pa.lexema + " en la linea: " + pa.linea);
            System.exit(0);
        } else if (pa.valorMT == 119) {
            VariablesCadena.add(pa.lexema);
            cont++;
            VT.add("cadena" + cont);
            valor_cadena();
        }
        if (pa.valorMT == 103) {
            Ingresar_Polish(pa);
            pa = pa.apuntador;
            if (pa.valorMT == 100 || pa.valorMT == 119) {
                concatenar();
            } else {
                System.out.println("Se esperaba variable o cadena en la linea " + pa.linea);
                System.exit(0);
            }
        }
    }

    public static void creartemporal(String cadena) {
        if (P1 == null) {
            P1 = new Temporales(cadena);
            Q1 = F1 = P1;
        } else {
            Q1 = new Temporales(cadena);
            F1.LigaD = Q1;
            Q1.LigaI = F1;
            F1 = Q1;
        }
    }
}
