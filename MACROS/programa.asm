.MODEL SMALL 
 .stack 
 .data 

a db "caracteres","$" 
b db 2,"$" 
T1 db 2,"$" 
c db 25,"$" 
T2 db 25,"$" 
d db 10,"$" 
T3 db 10,"$" 
e db "algo","$" 



 .code 
 nuevaLinea proc
mov dl, 10
 mov ah,02h
 int 21h
 mov dl,13
 mov ah, 02h
 int 21h 
 ret
nuevaLinea endp

 main proc
 
 mov al, T2
 cmp al, T3
 jc P
 mov ax,seg e
 mov ds,ax
 
 mov ah,09
 lea dx,e
 int 21h
 call nuevaLinea
 jmp Q
 P:
 mov ax,seg a
 mov ds,ax
 
 mov ah,09
 lea dx,a
 int 21h
 call nuevaLinea
 Q:
 S:
 mov al, T2
 cmp al, T3
 jc R
 mov ax,seg d
 mov ds,ax
 
 mov ah,09
 lea dx,d
 int 21h
 call nuevaLinea
 jmp S
 R:

main endp 
 end main