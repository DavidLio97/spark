.MODEL SMALL
.stack
.data
msg db "Hola Mundo","$"
len equ $ - msg

.code
main proc
    mov ax,seg msg
    mov ds,ax
    
    mov ah,09
    lea dx,msg
    int 21h
main endp
end main